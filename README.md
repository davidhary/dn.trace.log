# .NET Tracing and Logging  Libraries

Support libraries extending the Visual Studio framework for engineering and measurements.
This is a fork of the [dn.core] repository.

* [Source Code](#Source-Code)
* [License](LICENSE.md)
* [Change Log](CHANGELOG.md)
* [Facilitated By](#Facilitated-By)
* [Repository Owner](#Repository-Owner)
* [Acknowledgments](#Acknowledgments)
* [Open Source](#Open-Source)
* [Closed Software](#Closed-software)

## Source Code[](#){name=Source-Code}
Clone the repository along with its requisite repositories to their respective relative path.

### Repositories
The repositories listed in [external repositories] are required:
* [std] - ISR framework libraries for .NET Standard
* [trace.log] - ISR framework tracing and logging Libraries
```
git clone git@bitbucket.org:davidhary/dn.std.git
git clone git@bitbucket.org:davidhary/dn.trace.log.git
```

Clone the repositories into the following folders (parents of the .git folder):
```
%vslib%\core\std
%vslib%\core\trace.log
```
where %vslib% is the root folder of the .NET libraries, e.g., %my%\lib\vs 
and %my% is the root folder of the .NET solutions

#### Global Configuration Files
ISR libraries use a global editor configuration file and a global test run settings file. 
These files can be found in the [IDE Repository].

Restoring Editor Configuration:
```
xcopy /Y %my%\.editorconfig %my%\.editorconfig.bak
xcopy /Y %vslib%\core\ide\code\.editorconfig %my%\.editorconfig
```

Restoring Run Settings:
```
xcopy /Y %userprofile%\.runsettings %userprofile%\.runsettings.bak
xcopy /Y %vslib%\core\ide\code\.runsettings %userprofile%\.runsettings
```
where %userprofile% is the root user folder.

#### Packages
Presently, packages are consumed from a _source feed_ residing in a local folder, e.g., _%my%\nuget\packages_. 
The packages are 'packed', using the _Pack_ command from each packable project,
into the _%my%\nuget_ folder as specified in the project file and then
added to the source feed. Alternatively, the packages can be downloaded from the 
private [MEGA packages folder].

## Facilitated By[](#){name=Facilitated-By}

* [Visual Studio]
* [Jarte RTF Editor]
* [WiX Toolset]
* [Atomineer Code Documentation]
* [EW Software Spell Checker]
* [Code Converter]
* [Funduc Search and Replace]

## Repository Owner[](#){name=Repository-Owner}

[ATE Coder]

## Acknowledgments[](#){name=Acknowledgments}

* [Its all a remix] -- we are but a spec on the shoulders of giants
* [John Simmons] - outlaw programmer
* [Stack overflow] - Joel Spolsky

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[std]  
[trace.log]  
[Drop Shadow and Fade From]  
[Engineering Format]  
[Enumeration Extensions]  
[Enum.Net]
[Exception Extension]  
[FastEnum]
[High Resolution Clock]  
[Linq Statistics]  
[NameValueCollection] 
[Notification Window]  
[Read Write Lock Simple]
[Office Style Splash Screen]  
[Safe Copy from Clipboard]  
[Safe Events1]  
[Safe Events2]  
[Safe Events3]  
[Serilog]  
[Serilog.Enrichers.Demystify]  
[Serilog.Sinks.Async]  
[Serilog.Sinks.File]  
[Serilog.Sinks.File.Header]  
[Sorted Bucket Collection]  
[String Enumerator]
[Unconstrained Melody]

### Closed software  [](#){name=Closed-software}
Closed software used by this software are described and licensed on
the following sites:  
[SQL Exception Message]
 
### Links
[MEGA packages folder]: https://mega.nz/folder/KEcVxC5a#GYnmvMcwP4yI4tsocD31Pg
[std]: https://www.bitbucket.org/davidhary/dn.std
[trace.log]: https://www.bitbucket.org/davidhary/dn.trace.log
[Drop Shadow and Fade From]: http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx
[Engineering Format]: http://WallaceKelly.BlogSpot.com/
[Enum.Net]: https://github.com/TylerBrinkley/Enums.NET
[Enumeration Extensions]: https://www.codeproject.com/Articles/37921/Enums-Flags-and-Csharp-Oh-My-Bad-Pun
[Exception Extension]: https://www.codeproject.com/Tips/1179564/A-Quick-Dirty-Extension-Method-to-Get-the-Full-Exc
[FastEnum]: https://github.com/xin9le/FastEnum
[High Resolution Clock]: https://www.codeproject.com/articles/792410/high-resolution-clock-in-csharp  
[Linq Statistics]: http://www.codeproject.com/Articles/42492/Using-LINQ-to-Calculate-Basic-Statistics
[NameValueCollection]: https://www.codeproject.com/Articles/5323395/A-Generic-Form-of-the-NameValueCollection.
[Notification Window]: http://www.codeproject.com/KB/dialog/notificationwindow.aspx
[Office Style Splash Screen]: http://www.codeproject.com/Articles/804316/Office-Style-Splash-Screen
[Read Write Lock Simple]: https://www.codeproject.com/Tips/5323262/The-Simplest-Implementation-of-a-Reader-Writer-Loc
[Safe Copy from Clipboard]: http://stackoverflow.com/questions/899350/how-to-copy-the-contents-of-a-string-to-the-clipboard-in-c
[Safe Events1]: http://www.CodeProject.com/KB/cs/EventSafeTrigger.aspx
[Safe Events2]: http://www.DreamInCode.net/forums/user/334492-aeonhack
[Safe Events3]: http://www.DreamInCode.net/code/snippet5016.htm
[Serilog]: https://github.com/serilog/serilog
[Serilog.Enrichers.Demystify]: https://github.com/nblumhardt/serilog-enrichers-demystify
[Serilog.Sinks.Async]: https://github.com/serilog/serilog-sinks-async 
[Serilog.Sinks.File]: https://github.com/serilog/serilog-sinks-file
[Serilog.Sinks.File.Header]: https://github.com/cocowalla/serilog-sinks-file-header
[Sorted Bucket Collection]: https://www.codeproject.com/Articles/5317083/SortedBucketCollection-A-memory-efficient-SortedLi
[SQL Exception Message]: https://msdn.microsoft.com/en-us/library/ms365274.aspx
[String Enumerator]: http://www.codeproject.com/Articles/17472/StringEnumerator
[Unconstrained Melody]: https://github.com/jskeet/unconstrained-melody

[IDE Repository]: https://www.bitbucket.org/davidhary/vs.ide
[external repositories]: ExternalReposCommits.csv

[ATE Coder]: https://www.IntegratedScientificResources.com
[Its all a remix]: https://www.everythingisaremix.info
[John Simmons]: https://www.codeproject.com/script/Membership/View.aspx?mid=7741
[Stack overflow]: https://www.stackoveflow.com

[Visual Studio]: https://www.visualstudio.com/
[Jarte RTF Editor]: https://www.jarte.com/ 
[WiX Toolset]: https://www.wixtoolset.org/
[Atomineer Code Documentation]: https://www.atomineerutils.com/
[EW Software Spell Checker]: https://github.com/EWSoftware/VSSpellChecker/wiki/
[Code Converter]: https://github.com/icsharpcode/CodeConverter
[Funduc Search and Replace]: http://www.funduc.com/search_replace.htm
