# Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [1.0.8102] - 2022-03-08
* Fork of the [dn.core] repository.

(C) 2012 Integrated Scientific Resources, Inc. All rights reserved.

```
## Release template - [version] - [date]
## Unreleased
### Added
### Changed
### Deprecated
### Removed
### Fixed
*<project name>*
```
[dn.core]: https://www.bitbucket.org/davidhary/dn.core
[1.0.8102]: https://bitbucket.org/davidhary/dn.std/src/main/
