# The MIT License
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE.

## License Dependencies

[Serilog.Sinks.File -- Apache License 2.0](https://github.com/serilog/serilog-sinks-file/blob/dev/LICENSE)  
[Serilog.Sinks.File.Header -- Apache License 2.0](https://github.com/cocowalla/serilog-sinks-file-header/blob/master/LICENSE) 
[Serilog.Enrichers.Demystify -- Apache License 2.0](https://github.com/nblumhardt/serilog-enrichers-demystify)
[Unconstrained Melody -- Apache License 2.0](https://github.com/jskeet/unconstrained-melody)
[Sorted Bucket Collection -- Creative Common 0](http://creativecommons.org/publicdomain/zero/1.0/)
